import argparse
import boto3
import configparser
import os
from pathlib import Path


# класс программы для обработки и выполнения команд (кроме команды init)
class Cloudphoto:
    def __init__(self, args):
        self.functions = {"list": self.list,
                          "upload": self.upload,
                          "delete": self.delete,
                          "mksite": self.mksite}
        self.types = ["jpg", "jpeg", ]
        self.args = args

        # работа с конфиг. файлом
        open(f"{Path.home()}/.config/cloudphoto/cloudphotorc.ini", "r")
        config = configparser.ConfigParser()
        config.read(f"{Path.home()}/.config/cloudphoto/cloudphotorc.ini")

        # заполнение аргументов из конфигурационного файла
        if (
                config['DEFAULT']['bucket'] and
                config['DEFAULT']['aws_access_key_id'] and
                config['DEFAULT']['aws_secret_access_key'] and
                config['DEFAULT']['region'] and
                config['DEFAULT']['endpoint_url']
        ):
            self.bucket = config['DEFAULT']['bucket']
            self.aws_access_key_id = config['DEFAULT']['aws_access_key_id']
            self.aws_secret_access_key = config['DEFAULT']['aws_secret_access_key']
            self.region = config['DEFAULT']['region']
            self.endpoint_url = config['DEFAULT']['endpoint_url']
        else:
            raise AttributeError('Error with config file')

    # создание сессии для работы с сервисом
    def create_session(self):
        admin_session = boto3.session.Session(aws_access_key_id=self.aws_access_key_id,
                                              aws_secret_access_key=self.aws_secret_access_key, region_name=self.region)
        admin_resource = admin_session.resource(service_name='s3', endpoint_url=self.endpoint_url)
        bucket = admin_resource.Bucket(self.bucket)
        return bucket

    # определение введенной команды
    def find_command(self, command):
        do_command = self.functions.get(command)
        if do_command:
            do_command()
        else:
            raise AttributeError(f'Command <{command}> not found')

    # далее исполнение четырех команд: list, upload, delete, mksite

    def list(self):
        bucket = self.create_session()
        dir_list = get_list_of_albums(bucket)
        for dir_ in dir_list:
            print(dir_)

    def upload(self):
        if self.args.album:
            upload_bucket = self.create_session()
            if os.path.isdir(self.args.path):

                flag = False
                for filename in os.listdir(self.args.path):
                    if os.path.isfile(os.path.join(self.args.path, filename)) and filename.split(".")[1] in self.types:

                        try:
                            uploader_object = upload_bucket.Object(f"{self.args.album}/{filename}")
                            uploader_object.upload_file(Filename=os.path.join(self.args.path, filename))
                            flag = True
                        except Exception:
                            raise RuntimeError(f'Warning: Photo not sent <{filename}>')

                if not flag:
                    raise RuntimeError(f'Warning: Photos not found in directory <{self.args.path}>')

            else:
                raise RuntimeError(f'Warning: No such directory <{self.args.path}>')

        else:
            raise AttributeError('Album name was not given')

    def delete(self):
        if self.args.album:
            bucket = self.create_session()

            flag = False
            for obj in bucket.objects.all():
                if self.args.album in obj.key:
                    obj.delete()
                    flag = True

            if not flag:
                raise RuntimeError(f'Warning: Photo album not found <{self.args.album}>')

        else:
            raise AttributeError('Album name was not given')

    def mksite(self):
        bucket = self.create_session()
        dir_list = get_list_of_albums(bucket)
        bucket.Acl().put(ACL='public-read')
        bucket_website = bucket.Website()

        index_page = {'Suffix': 'index.html'}
        error_page = {'Key': 'error.html'}
        bucket_website.put(WebsiteConfiguration={'ErrorDocument': error_page, 'IndexDocument': index_page})

        html_object = bucket.Object('index.html')
        html_object.put(Body=generate_index_page(dir_list), ContentType='text/html')
        html_object = bucket.Object('error.html')
        html_object.put(Body=generate_error_page(), ContentType='text/html')

        for index, dir_ in enumerate(dir_list):
            html_object = bucket.Object(f'album{index}.html')
            html_object.put(Body=generate_album_page(dir_, bucket), ContentType='text/html')

        print(f"https://{bucket.name}.website.yandexcloud.net")


# генерация главной страницы
def generate_index_page(dir_list):
    page = "<!DOCTYPE><html><head><title>Photo archive</title></head><body><h1>Photo archive</h1><ul>"

    for index, dir_ in enumerate(dir_list):
        page += f'<li><a href="album{index}.html">{dir_}</a></li>'
    page += "</ul></body>"

    return page


# генерация страницы ошибки
def generate_error_page():
    return '<!DOCTYPE><html><head><title>Photo archive</title></head><body><h1>Error</h1><p>Error accessing photo archive. Return to <a href="index.html">home page</a> of photo archive.</p></body></html>'


# генерация страницы с альбомом
def generate_album_page(dir_, bucket):
    page = '<!DOCTYPE><html><head><link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/themes/classic/galleria.classic.min.css" /><style>.galleria{ width: 960px; height: 540px; background: #000 }</style><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/galleria.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/galleria/1.6.1/themes/classic/galleria.classic.min.js"></script></head><body><div class="galleria">'

    for photo in bucket.objects.all():
        if dir_ in photo.key:
            page += f'<img src={photo.key} data-title="{photo.key.split("/")[1]}">'
    page += '</div><p>Go back to <a href="index.html">main page</a> of photo archive</p><script>(function() {Galleria.run(".galleria");}());</script></body></html>'

    return page


# создание аргументов командной строки
def create_args():
    parser = argparse.ArgumentParser(description='CloudPhoto argument parser')

    parser.add_argument('command', type=str, help='Input command for CloudPhoto script')
    parser.add_argument('-a', '--album', type=str, help='Name of the album')
    parser.add_argument('-p', '--path', type=str, help='Path to the album with photos', default=".")
    args = parser.parse_args()

    return args


# создание списка альбомов
def get_list_of_albums(bucket):
    list_of_album = []
    objects = sorted(obj.key for obj in bucket.objects.all())
    if objects:
        for obj in objects:
            if "/" in obj and obj.split('/')[0] not in list_of_album:
                list_of_album.append(obj.split('/')[0])

        return list_of_album

    else:
        raise RuntimeError('Photo albums not found')


# выполнение команды init
def init():
    aws_access_key_id = input("Введите айди ключа: ")
    aws_secret_access_key = input("Введите секретный ключ: ")
    input_bucket = input("Введите название бакета: ")

    admin_session = boto3.session.Session(aws_access_key_id=aws_access_key_id,
                                          aws_secret_access_key=aws_secret_access_key, region_name="ru-central1")
    admin_resource = admin_session.resource(service_name='s3', endpoint_url='https://storage.yandexcloud.net')

    if input_bucket not in (bucket.name for bucket in admin_resource.buckets.all()):
        admin_resource.Bucket(input_bucket).create()

    create_init_file(input_bucket, aws_access_key_id, aws_secret_access_key)


# слздание конфигурационного файла при выполнении команды init
def create_init_file(input_bucket, aws_access_key_id, aws_secret_access_key):
    config = configparser.ConfigParser()

    config['DEFAULT'] = {'region': 'ru-central1',
                         'endpoint_url': 'https://storage.yandexcloud.net'}
    config['DEFAULT']['bucket'] = input_bucket
    config['DEFAULT']['aws_access_key_id'] = aws_access_key_id
    config['DEFAULT']['aws_secret_access_key'] = aws_secret_access_key

    with open(f"{Path.home()}/.config/cloudphoto/cloudphotorc.ini", "w") as ini_file:
        config.write(ini_file)


def main():
    args = create_args()
    command = args.command
    if command == "init":
        init()
    else:
        cloudphoto = Cloudphoto(args)
        cloudphoto.find_command(command)


if __name__ == '__main__':
    main()
